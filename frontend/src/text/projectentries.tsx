// import erasmuslogo from "./images/erasmuslogo.jpg";
export type languageType = "EN" | "NL";
export type projectType = {
    title: { EN: string; NL: string };
    stack: string[];
    description: { EN: string; NL: string };
    image: string;
    link: string;
};

export const projectData = [
    {
        title: {
            EN: "Vacancy Platform Rotterdam Werkt!",
            NL: "Vacature Platform Rotterdam Werkt!",
        },
        // "Vacature Platform Rotterdam Werkt!",
        stack: ["Django", "Python", "React", "ELK-Stack"],
        description: {
            EN: "A centralized search-engine platform that fills the vacancies of the private network Rotterdam Works!, that were scraped from all their websites and then displays. Personally responsible for the implementation of the API and Database",
            NL: "Een gecentraliseerd search-engine platform die de vacatures van het prive netwerk Rotterdam Werkt! scraped, vanuit al hun websites en vervolgens weergeeft. Persoonlijk verantwoordelijk voor de implemenatie = implementatie de API en Database",
        },
        image: "./images/erasmuslogo.png ",
        link: "../files/Rotterdam_Werkt.pdf",
    },
    {
        title: {
            EN: "Spotivibes",
            NL: "Spotivibes",
        },
        stack: ["Flask", "Python", "React"],
        description: {
            EN: "Recommender System for Spotify returns new numbers in relation to your musical taste.",
            NL: "Recommender System voor Spotify, geeft nieuwe nummers op basis van luistersmaak.  =luister smaak",
        },
        image: "./images/spotify.png ",
        link: "https://www.youtube.com/watch?v=x2KZ2z0s4Uk&ab_channel=habjanse",
    },
    {
        title: {
            EN: "Balashi Aruba",
            NL: "Balashi Aruba",
        },
        stack: ["Flask", "Python", "React"],
        description: {
            EN: "Website for a vacation home in Aruba. Integrated email server made in Flask.",
            NL: "Website voor vakantiehuis op  Aruba. Ingebouwde email server in Flask.",
        },
        image: "./images/balashi.png ",
        link: "https://www.balashi-aruba.com",
    },
    {
        title: {
            EN: "Assembler Encryptor",
            NL: "Assembler Encryptie",
        },
        stack: ["x86 At&t Assembler"],
        description: {
            EN: "Foto encryption software made for hardware systems.",
            NL: "Foto encryptie software voor hardware systemen.",
        },
        image: "./images/hardware.png ",
    },
    {
        title: {
            EN: "Lisence Plate Scanner",
            NL: "NummerPlaat Scanner",
        },

        stack: ["Python", "OpenCV"],
        description: {
            EN: "Lisence plate scanner. Returns lisence plate as text based on video input.",
            NL: "Nummerplaat scanner. Geeft nummerplaat terug als text op basis van video.",
        },
        image: "./images/spotify.png ",
    },
] as projectType[];
export const workedWith = {
    NL: "Bedrijven waar we mee hebben gewerkt:",
    EN: "Companies we have worked with: ",
} as { NL: string; EN: string };

export type servicesType = {
    title: { EN: string; NL: string };
    description: { EN: string; NL: string };
    bullets: { EN: string[]; NL: string[] };
    image?: string;
};

export const services = [
    {
        title: {
            NL: "Web Applicaties",
            EN: "Web Applications",
        },
        description: {
            NL: "Het bouwen van full stack applicaties/systemen. Het front-end systeem zowel het back-end systeem wordt door ons gebouwd.",
            EN: "We design and create full stack web applications, meaning that we design both the front-end and the back-end of the web application. An example would be a user interface (Front-end) for both the clients and the administrator, as well as the system that links them together (Back-end), think of a database and central servers.",
        },
        bullets: {
            NL: [
                "Gebruiker Authenticatie",
                "Geautomatiseerde Functionaliteiten",
                "Front-end: React, CSS, Javascript, Typescript",
                "Back-end: Django, Python, PostGres, Linux, Celery",
                "Database: PostgreSQL",
            ],
            EN: [
                "User Authentication",
                "Automated functionalities",
                "Front-end: HTML, CSS, Javascript",
                "Back-end: Django, Python, PostGres, Linux, Celery",
                "Database: PostgreSQL",
            ],
        },
        image: "./images/web_app.png",
    },
    {
        title: {
            NL: "UI / UX Ontwerp",
            EN: "UI / UX Design",
        },
        description: {
            NL: "Onderzoek laat zien dat vele gebruikers een website verlaten als de voorpagina er niet mooi uitziet. Daarom leggen we hierop de nadruk.",
            EN: "Research shows that many users leave their websites if the front page isn’t pleasing to look at. Therefore we put a high emphasis on this particular feature.",
        },
        bullets: {
            NL: [
                "Mobiel vriendelijk web ontwerp",
                "Responsive web ontwerp",
                "Gebruikersomgeving ontwerp (UI)",
                "Gebruikersinteractie ontwerp (UX)",
                "Website herontwerp",
                "Webshop web ontwerp",
            ],
            EN: [
                "Mobile friendly web-design",
                "Responsive web-design",
                "User interface design (UX)",
                "User experience design (UX)",
                "Website redesign",
                "E-commerce web design",
            ],
        },
    },

    {
        title: {
            NL: "Consultatie",
            EN: "Consulting",
        },
        description: {
            NL: "Wij geven technologisch advies aan over verschillende onderwerpen hier onder benoemd.",
            EN: "We provide technologic advice about different subjects which are listed below.",
        },
        bullets: {
            NL: [
                "Hosting",
                "Het digitaliseren van uw bedrijf",
                "Softwarekwaliteit",
                "Front- en Back-end Raamwerken",
                "Software architectuur",
            ],
            EN: [
                "Hosting",
                "Digitalization",
                "Software quality",
                "Front- and Back-end Frameworks",
                "Software architecture",
            ],
        },
        image: "./images/consult.png",
    },
    {
        title: {
            NL: "SEO en Advertenties",
            EN: "SEO and Ads",
        },
        description: {
            NL: "Meer dan 92 procent van de zoekmachine markt wordt gedomineerd door Google. Het gemiddeld rendement op een online advertentie is 118% bovenop de oorspronkelijke investering.”Daarom hebben wij ons gespecialiseerd in het optimaliseren van de zoekmachine (SEO) en online advertenties.",
            EN: "More than 92 percent of the search engine market is dominated by Google. The average return on investment for online advertising was an additional 118% on top of the original investment.” For these reasons we have specialized in Search Engine Optimization (SEO) and Ads.",
        },
        bullets: {
            NL: [
                "Google zoekmachine optimalisatie",
                "Facebook advertenties",
                "Google advertenties",
                "Instagram advertenties",
                "Linkedin advertenties",
            ],
            EN: [
                "Google Search Optimization",
                "Facebook ads",
                "Google ads",
                "Instagram ads",
                "Linkedin ads",
            ],
        },
    },
    {
        title: {
            NL: "Infrastructureel Aanbod",
            EN: "Infrastructural Services",
        },
        description: {
            NL: "Wij bieden de aanleg van de volgende infrastructurele systemen aan.",
            EN: "We offer the installation of the following infrastructural systems.",
        },
        bullets: {
            NL: [
                "Aanleg van beveiligingsapparatuur",
                "Optimaliseren en aanleg van Wifi Access Points",
                "Aanleg van een netwerk",
                "Wifi Installatie",
            ],
            EN: [
                "Security Infrastructure",
                "Optimization and Installation of Wifi Access Points",
                "Network Infrastructure",
                "Wifi Installation",
            ],
        },
    },
    {
        title: {
            NL: "CI/CD",
            EN: "CI/CD",
        },
        description: {
            NL: "We vinden het van belang dat onze software snel en veilig ingezet kan worden. Om deze reden zijn onze servers onderhevig aan voortdurende testen. Oftewel CI/CD. ",
            EN: "We believe in the fast deployment and the continuous testing of our software, which is why we have implemented Continuous testing and continuous deployment infrastructures which allows for quick change of our products and the assurance of their quality.",
        },
        bullets: {
            NL: ["Git / Gitlab", "Jenkins", "Docker"],
            EN: ["Git / Gitlab", "Jenkins", "Docker"],
        },
    },
] as servicesType[];

export const contact = {
    placeholders: {
        name: {
            NL: "Naam",
            EN: "Name",
        },
        company: {
            NL: "Bedrijf",
            EN: "Company",
        },
        email: {
            NL: "Email",
            EN: "Email",
        },
        phone: {
            NL: "Telefoonnummer",
            EN: "Phone number",
        },
        message: {
            NL: "Uw bericht",
            EN: "Message",
        },
        submit: {
            NL: "Verstuur",
            EN: "Submit",
        },
    },
    error: {
        name: {
            NL: "Naam is een verplicht veld",
            EN: "Name is a required field",
        },
        email: {
            NL: "Email is een verplicht veld",
            EN: "Email is a required field",
        },
        message: {
            NL: "Bericht is een verplicht veld",
            EN: "Message is a required field",
        },
        phone: {
            NL: "Graag een geldig telefoonnummer invoeren",
            EN: "Please fill in a valid phone number",
        },
    },
};

export const footer = {
    phone: {
        NL: "Telefoonnummer",
        EN: "Phone Number",
    },
} as { phone: { NL: string; EN: string } };

export const navbar_text = {
    home: {
        NL: "Home",
        EN: "Home",
    },
    services: {
        NL: "Aanbod",
        EN: "Services",
    },

    contact: {
        NL: "Contact",
        EN: "Contact",
    },
    projects: {
        NL: "Projecten",
        EN: "Projects",
    },
};

export const filler = {
    title: {
        NL: "Software Op Maatwerk.",
        EN: "Custom Software.",
    },
    description: {
        NL: "Wij bouwen software afhankelijk aan wat u nodig heeft.",
        EN: "We build software based on your needs.",
    },
};
