export const projects_text = {
    title: { EN: "Example Web Applications", NL: "Voorbeeld Web Applicaties" },

    projects: [
        {
            name: {
                NL: "Het Reserverings Platform",
                EN: "The Booking System",
            },
            description: {
                NL: "Dit reserverings systeem wordt geintegreert in uw website. De gebruiker wordt dus niet naar een aparte website gestuurd. Verder kan er in dit systeem allerlei stappen worden toegevoegd, denk aan soort reservering, tijd, betaling systemen.",
                EN: "This reservation system is integrated into your website. The user is therefore not sent to a separate website. Furthermore, all kinds of steps can be added to this system, such as type of reservation, time and/or payment systems. ",
            },
            image: "images/website.png",
        },
        {
            name: {
                NL: "Het Factuur Platform",
                EN: "The Invoice System",
            },
            description: {
                NL: "Het factuur systeem maakt facturen, dit kan automatisch doorgestuurd worden naar klanten via email zodra een aankoop is gemaakt. Verder kan dit systeem alle facturen opslaan in een database waar alleen de beheerders van de website deze terug kunnen downloaden.",
                EN: "The invoice system creates invoices, which can be automatically forwarded to customers via email once a purchase is made. Furthermore, this system can store all invoices in a database where only the administrators of the website can download them. ",
            },
            image: "images/website.png",
        },
        {
            name: {
                NL: "Het Vacature Platform",
                EN: "The Vacancy Platform",
            },
            description: {
                NL: "Ingebouwde zoekmachine, scraper en sociaal netwerk. Gebouwd om bedrijven in een netwerk vacatures van elkaar te vinden.",
                EN: "Built-in search engine, scraper and social network. Built to help companies in a network find vacancies from each other.",
            },
            image: "images/website.png",
        },
    ],
};
