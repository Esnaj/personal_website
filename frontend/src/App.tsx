import { Home } from "./pages/home";
import { Services } from "./pages/services";
import { Companies } from "./pages/companies";
import Footer from "./pages/footer";
import { ContactForm } from "./pages/form";
import Navbar from "./components/Navbar";
import { createRef, RefObject } from "react";
import { useLanguage, availableLangs } from "./LanguageContext";
import { navbar_text } from "./text/projectentries";
import { Portfolio } from "./pages/portfolio";
import Filler from "./pages/filler";
// import Test from "./pages/test";

function App() {
    const homeRef = createRef<HTMLDivElement>();
    const servicesRef = createRef<HTMLDivElement>();
    const projectsRef = createRef<HTMLDivElement>();
    const contactRef = createRef<HTMLDivElement>();
    const currentLang = useLanguage() as availableLangs;
    var items: [string, RefObject<HTMLDivElement>][] = [
        [navbar_text["home"][currentLang], homeRef],
        [navbar_text["services"][currentLang], servicesRef],
        [navbar_text["projects"][currentLang], projectsRef],
        [navbar_text["contact"][currentLang], contactRef],
    ];

    return (
        <div className="items-center font-lora">
            <Navbar items={items} />
            <Home ref={homeRef} />
            <div className="flex flex-row w-full justify-center">
                <div className="flex flex-col items-center w-full lg:w-3/4 px-8 relative font-avenir">
                    <Companies />
                    <Services ref={servicesRef} />
                    <Filler />
                    <Portfolio ref={projectsRef} />
                    <ContactForm ref={contactRef} />
                    <Footer />
                </div>
            </div>
        </div>
    );
}

export default App;
