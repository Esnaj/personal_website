type servicesType = {
    title: { EN: string; NL: string };
    description: { EN: string; NL: string };
    image: string;
};

export const servicesText = [
    {
        title: {
            NL: "Hosting",
            EN: "Hosting",
        },
        description: {
            NL: "Websites kunnen gehost worden door Esnaj Software. SSL encryptie is gratis leverbaar voor een veilige https:// connectie.",
            EN: "Any website built can be hosted by Esnaj Software. SSL encryption included providing a secure https:// connection",
        },
    },
    {
        title: {
            NL: "Web Applicaties",
            EN: "Web applications",
        },
        description: {
            NL: "Naast websites bouwen we niet alleen simpele websites maar ook applicaties/systemen.",
            EN: "Building online applications. Consists of ",
        },
    },
    {
        title: {
            NL: "Consultatie",
            EN: "Consulting",
        },
        description: {
            NL: "Technologisch advies hier beschikbaar. Specialiteiten: hosting, servers, backend raamwerken, frontend raamwerken en software kwaliteit.",
            EN: "Technologic advice is available here. Specialties inclued: hosting, servers, backend frameworks, frontend framewokrs and software quality.",
        },
        image: ""
    },
] as servicesType[];
