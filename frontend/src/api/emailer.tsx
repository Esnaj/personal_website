import axios from "axios";
import { formType } from "../pages/form";

const api_url =
    !process.env.NODE_ENV || process.env.NODE_ENV === "development"
        ? "http://localhost:5000/"
        // : "email.esnajsoftware.com";
        :window.location.href + "api/"

export async function postForm(data: formType) {
    // api_url = window.location.href
    console.log(api_url);
    return axios
        .post(api_url, data)
        .then((res: any) => {
            // const data = res.data;
        })
        .catch((error: any) => {
            console.log(error);
        });
}
