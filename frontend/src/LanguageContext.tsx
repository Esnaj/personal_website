import { createContext, ReactNode, useContext, useState } from "react";

export type availableLangs= "NL" | "EN"
const LanguageContext = createContext("NL")
const LanguageUpdateContext = createContext((e : availableLangs) => {})




export function useLanguage() {
    return useContext(LanguageContext)
}

export function useLanguageUpdate() {
    return useContext(LanguageUpdateContext)
}
export function languageHandler() {

}

export function LanguageProvider(props :{children: ReactNode}) {
    const [language, setLanguage] = useState("NL")
    
    return (
        <LanguageContext.Provider value={language}>
            <LanguageUpdateContext.Provider value={(e : availableLangs) => setLanguage(e)}>
                {props.children}
            </LanguageUpdateContext.Provider>
        </LanguageContext.Provider>

    )
}