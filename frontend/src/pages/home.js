import { useLanguage } from "../LanguageContext";
import "./css/home.scss";
import { home_text as home_text_langs } from "./text";
import { useState, useEffect, useCallback, forwardRef } from "react";
import BIRDS from "vanta/dist/vanta.birds.min.js";
import { useWindowDimensions } from "../components/Utils";

export const Home = forwardRef((props, ref) => {
    const [vantaEffect, setVantaEffect] = useState(0);
    let home_text = home_text_langs[useLanguage()];

    const { width } = useWindowDimensions();
    const isMobileCallback = useCallback(() => {
        return width <= 768;
    }, [width]);

    useEffect(() => {
        if (!vantaEffect) {
            setVantaEffect(
                BIRDS({
                    el: ref.current,
                    mouseControls: true,
                    touchControls: true,
                    gyroControls: false,
                    // minHeight: 1000.0,
                    // minWidth: 1400,
                    scale: 1.0,
                    scaleMobile: 1.,
                    backgroundColor: 0x021e4d,
                    backgroundAlpha: 0,
                    colorMode: "variance",
                    birdSize: isMobileCallback() ? 2.0 : 4.7,
                    wingSpan: 30.0,
                    speedLimit: 3.0,
                    separation: isMobileCallback() ? 1000 : 250.0,
                    alignment: -10.0,
                    cohesion: 0.0,
                    quantity: isMobileCallback() ? 1.0 : 2.0,
                }),
            );
        }
        return () => {
            if (vantaEffect) vantaEffect.destroy();
        };
    }, [vantaEffect, isMobileCallback, ref]);
    return (
        <div
            ref={ref}
            id="aboutmain"
            className="w-screen flex flex-row justify-center items-center">
            <div className="z-10 flex flex-col rounded-md p-3 mx-3 bg-opacity-80 md:bg-opacity-10 text-white bg-fixed">
                <div id="esnaj_software_container" className="text-6xl mb-12 md:mb-28 ">
                    <div id="esnaj_container">
                        <div id="e">E</div>
                        <div className="text_container">
                            <div id="snaj"> snaj </div>
                        </div>
                    </div>
                    <div id="software_container" className="">
                        <div id="s"> S</div>
                        <div className="text_container">
                            <div id="oftware"> oftware </div>
                        </div>
                    </div>
                </div>
                <div className="text-4xl md:text-6xl font mb-6">
                    <div className="inline-block ">
                        <span id="webapp" className="whitespace-nowrap z-30 relative ">
                            {home_text["webapp"]}
                        </span>
                        <span className="inline"> {home_text["needed"]}? </span>
                    </div>
                    <br />
                </div>
                <span className="text-3xl text-gray-600">
                    {/* Esnaj software {home_text["dlvr1"]} */}
                    {/* <span className=""> {home_text["dlvr2"]} </span> */}
                    {/* {home_text["dlvr3"]} */}
                </span>
            </div>
        </div>
    );
});
