import { footer } from "../text/projectentries";
import { useLanguage, availableLangs } from "../LanguageContext";

export default function Footer() {
    const currentLang = useLanguage() as availableLangs;
    return (
        <footer className="bg-blue-900 text-gray-200 w-screen flex justify-center pb-10 pt-10 text-lg">
            <div className="flex flex-row md:w-2/5 ">
                <div className="flex flex-col">
                    <span>© Esnaj Software - 2021</span>
                    <span>KvK: 82094659</span>
                    <div>
                        <span>{footer["phone"][currentLang]}: </span>{" "}
                        <span className="whitespace-nowrap">+31 6 14521168</span>
                    </div>
                    <span>
                        Email: <a href="mailto:henky@esnajsoftware.com">henky@esnajsoftware.com </a>
                    </span>
                </div>
                {/* <div className="flex flex-col "></div> */}
            </div>

        </footer>
    );
}
