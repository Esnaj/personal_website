import "../index.css";
import erasmus_logo from "../images/erasmuslogo.jpg";
import spotify_logo from "../images/spotify.png";
import { JsxElement } from "typescript";
import React, { useEffect, useRef, useState } from "react";
import { LeftEntranceAnimation } from "../components/Sections";

function Experience() {
  let vacature_content =
    "Een gecentraliseerd search-engine platform die de vacatures van het prive netwerk Rotterdam Werkt! scraped, vanuit al hun websites en vervolgens weergeeft. Persoonlijk verantwoordelijk voor de implemenatie de API en Database";
  let spotivibes_content =
    "Een Recommender system, persoonlijk weer voornamelijk verantwoordelijk voor API.";
  return (
    <div className="w-full p-5 mt-5 rounded-md flex flex-col items-center">
      <div className="py-5 px-20 rounded-md">
        <h1 className="text-6xl w-1/2 text-center my-5">Projecten</h1>
      </div>
      <div className="flex flex-col align-center mx-auto">
        <ExperienceEntry
          title="Vacature Platform"
          subtitle="Voor prive netwerk Rotterdam Werkt!"
          content={vacature_content}
          image={erasmus_logo}
          techs={[
            "Python",
            "React",
            "Elastick Stack",
            "Django",
            "Django-Rest-FrameWork",
          ]}
        ></ExperienceEntry>
        <ExperienceEntry
          title="Spotivibes"
          subtitle="Recommender System voor Spotify"
          content={spotivibes_content}
          image={spotify_logo}
          techs={["Python", "Flask", "Javascript"]}
        ></ExperienceEntry>
        <ExperienceEntry
          title="Vacature Platform"
          subtitle="Voor prive netwerk Rotterdam Werkt!"
          content="asclkasjcnkj"
          image={spotify_logo}
        ></ExperienceEntry>
        <ExperienceEntry
          title="Vacature Platform"
          subtitle="Voor prive netwerk Rotterdam Werkt!"
          content="asclkasjcnkj"
        ></ExperienceEntry>
      </div>
    </div>
  );
}

function ExperienceEntry(props: {
  title: String;
  subtitle?: String;
  content: String;
  image?: any;
  techs?: String[];
}) {
  var stack_jsx = <></>;
  if (props.techs != null) {
    stack_jsx = create_stack_jsx(props.techs);
  }

  return (
    <LeftEntranceAnimation>
      <div className="bg-primary">
        <div className="absolute float-left">
          <div className="bg-secondary h-10 w-20 right-3 rounded-lg absolute z-30"></div>
        </div>
        <div className="bg-white my-8 h-auto w-2/3 flex flex-row z-10 p-3 rounded-md shadow-lg">
          <img
            src={props.image}
            className="object-contain h-auto w-20 mr-3"
          ></img>
          <div className="flex flex-col justify-start w-full">
            <div className="h-10 w-full pt">
              <span className="text-2xl text-gray-700 font-bold	float-left">
                {props.title}
              </span>
            </div>
            <span className="text-gray-700 text-l font-semibold float-left text-gray-800 ">
              {props.subtitle}
            </span>
            <span className="text-gray-600">{props.content}</span>
            {stack_jsx}
          </div>
        </div>
      </div>
    </LeftEntranceAnimation>
  );
}

function create_stack_jsx(techs: String[]) {
  return (
    <div className="inset-x-0 mt-auto">
      <span className="text-l font-semibold">De Stack: </span>
      <span>{techs.join(", ")}</span>
    </div>
  );
}

function Transition(props: { children: React.ReactChild }) {
  return (
    <div className="transition duration-500 ease-in-out hover: transform hover:-translate-y-1 hover:scale-110">
      {props.children}
    </div>
  );
}

export default Experience;
