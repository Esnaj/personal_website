import { useLanguage, availableLangs } from "../LanguageContext";
import "./css/companies.scss";
import { workedWith } from "../text/projectentries";


export function Companies() {
    const text = workedWith[useLanguage() as availableLangs];
    const companies = [
        "resources/svg/cgi.svg",
        "resources/svg/engie.svg",
        "resources/svg/evides.svg",
        "resources/svg/tudelft.svg",
        "resources/svg/facilicom.svg",
        "resources/svg/vopak.svg",
    ];
    return (
        <div className="companies w-screen  bg-red flex flex-col items-center my-14 ">
            <h1 className="md:text-3xl text-gray-500 text-center">{text}</h1>
            <div className="splitter mt-3" />
            <div className="w-2/3 mt-8 grid grid-cols-3 md:grid-cols-6 gap-3 auto-rows-max">
                {companies.map((e, i) => (
                    <img key={i} className="img" src={e} alt="company" />
                ))}
            </div>
        </div>
    );
}
