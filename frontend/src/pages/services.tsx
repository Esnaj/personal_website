import { forwardRef, useState } from "react";
import { useLanguage, availableLangs } from "../LanguageContext";
import { services, servicesType } from "../text/projectentries";
import "./css/services.scss";
import { useWindowDimensions } from "../components/Utils";
import Slider from "react-slick";

export const Services = forwardRef<HTMLDivElement>((props, ref) => {
    const currentLang = useLanguage() as availableLangs;
    const [activeService, setActiveService] = useState<number>(0);

    const { width } = useWindowDimensions();

    return (
        <div id="services_ref_wrap" className="md:w-full font-avenir" ref={ref}>
            {isMobile() ? <MobileView /> : <DesktopView />}
        </div>
    );

    function MobileView() {
        const settings = {
            infinite: true,
            adaptiveHeigt: true,
            initialSlide: activeService,
            speed: 500,
            centerPadding: "100px",
            slidesToShow: 1,
            slidesToScroll: 1,
            afterChange: (index: number) => {
                setActiveService(index);
            },
            className: "height: 100%;",
        };
        return (
            <div id="mobile_slider_container" className="w-screen mt-10 ">
                <Slider {...settings}>
                    {services.map((e: servicesType, i) => (
                        <div
                            id={"service_card_" + i}
                            key={i}
                            className="h-full flex flex-row h-84 justify-center items-center px-8">
                            <div className="w-full border shadow-md h-full">
                                <div className="p-6">
                                    <ServiceEntry
                                        title={e.title[currentLang]}
                                        description={e.description[currentLang]}
                                        bullets={e.bullets[currentLang] ?? []}
                                    />
                                </div>
                            </div>
                        </div>
                    ))}
                </Slider>
                <ServiceTabIndicators />
            </div>
        );
    }
    function DesktopView() {
        return (
            <div className="flex flex-row w-full justify-center">
                <div
                    id="service_container"
                    className="flex flex-col w-full md:flex-row h-full border shadow-md font-avenir">
                    <ServiceButtonContainer />

                    <div className="min-w-3/4 w-3/4 p-3">
                        {services.map(
                            (e: servicesType, i) =>
                                activeService === i && (
                                    <ServiceEntry
                                        key={i}
                                        title={e.title[currentLang]}
                                        description={e.description[currentLang]}
                                        bullets={e.bullets[currentLang] ?? []}
                                    />
                                ),
                        )}
                    </div>
                </div>
            </div>
        );
    }

    function ServiceTabIndicators() {
        return (
            <div className="flex flex-row space-x-3 justify-center mt-3 mb-3">
                {services.map((e, i) => {
                    const active_classes = "border-3 border-pink-300 bg-pink-300";
                    const css_classes = [
                        "border border-gray-300 bg-gray-300 w-10",
                        activeService === i && active_classes,
                    ].join(" ");
                    return <div key={i} className={css_classes} />;
                })}
            </div>
        );
    }

    function ServiceButtonContainer() {
        return (
            <div
                id="service_titles"
                className="hidden md:flex flex-col bg-blue-900 overflow-visible space-y-2 rounded-sm p-2 shadow-xl text-white text-xl relative right-2 bottom-2">
                {services.map((e: servicesType, i) => (
                    <ServiceButton id={i} key={i} title={e.title[currentLang]} />
                ))}
            </div>
        );
    }

    function ServiceEntry(props: { title: string; description: string; bullets: string[] }) {
        return (
            <div
                id={"service_entry_" + props.title}
                className="p-3 px-6 sm:px-10 w-full flex flex-col space-y-2 service_entry">
                <h1 className="text-2xl font-bold text-gray-600">{props.title}</h1>
                <div className="border w-24 border-pink-600 bg-pink-600" />
                <div className="description text-gray-600">
                    <span>{props.description}</span>
                    <ul className="pt-6 text-base">
                        {props.bullets.map((e: string, i) => (
                            <li className="service_bullet" key={i}>
                                <div className="bullet"></div>
                                <div className="bullet_text">{e}</div>
                            </li>
                        ))}
                    </ul>
                </div>
            </div>
        );
    }

    function ServiceButton(props: { id: number; title: string }) {
        const [isHover, setIsHover] = useState<boolean>(false);
        const isActive = activeService === props.id;
        return (
            <div
                className={["relative service_button", isActive && "bg-blue-300 shadow-lg "].join(
                    " ",
                )}>
                <button
                    className={[
                        "self-start float-left text-2xl font-bold ml-3 whitespace-normal p-3 pr-10 w-full text-left ",
                        isActive && "text-gray-800",
                    ].join(" ")}
                    onClick={() => {
                        setActiveService(props.id);
                    }}
                    onMouseEnter={() => setIsHover(true)}
                    onMouseLeave={() => setIsHover(false)}>
                    <span className={isHover && !isActive ? "service_button_content" : ""}>
                        {props.title}
                    </span>
                </button>
            </div>
        );
    }

    function isMobile() {
        return width <= 768;
    }
});
