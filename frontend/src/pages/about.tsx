import { availableLangs, useLanguage } from "../LanguageContext";
import { about_text } from "./text";

export function About(): JSX.Element {
    const text = about_text[useLanguage() as availableLangs];
    return (
        <div className="w-full mt-20">
            <div className="flex flex-col items-center">
                <Member name="Henky Janse" />
            </div>
        </div>
    );

    function Member(props: { name: string }) {
        return (
            <div className="flex flex-row bg-gray-100 w-2/3 p-3 space-x-10 h-auto">
                <div className="flex flex-col ">
                    <h1 className="text-3xl ">{props.name}</h1>
                    <span className="mt-2">
                        {text['text']}
                    </span>
                </div>
            </div>
        );
    }
}
