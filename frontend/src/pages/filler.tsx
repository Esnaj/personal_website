import { availableLangs, useLanguage } from "../LanguageContext";
import { filler } from "../text/projectentries";

export default function Filler(props: {}) {
    const currentLang = useLanguage() as availableLangs;
    return (
        <div className="w-screen bg-gray-700 bg-opacity-80  my-24 flex flex-row items-center justify-center h-60">
            <div className="px-8 md:px-0 md:w-1/2 text-center text-gray-100  ">
                <h1 className="text-4xl italic font-semibold mb-2 ">{filler.title[currentLang]}</h1>
                <span className="text-2xl text-center">
                    {filler.description[currentLang]}
                </span>
            </div>
        </div>
    );
}
