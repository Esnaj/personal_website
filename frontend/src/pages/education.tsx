import "../index.css";
import tudelft_logo from "../images/tudelft.jpg";
import colegio_logo from "../images/logo.png";
import { LeftEntranceAnimation } from "../components/Sections";

function Education() {
  var bsc_content =
    "Geleerd over computers very fun swag swag swag omg how much swag i don't know din the world is it me? no not even close, for there is jusaksjdnvckjandsvkjank";
  return (
    <div className="w-full">
      <h1 className="text-4xl w-1/2">Education</h1>
      <div className="flex flex-col align-center mx-auto">
        <EducationEntry
          title="TU Delft"
          subtitle="Bsc Computer Science"
          content={bsc_content}
          image={tudelft_logo}
        ></EducationEntry>
        <EducationEntry
          title="TU Delft"
          subtitle="Minor Transport"
          content={bsc_content}
          image={tudelft_logo}
        ></EducationEntry>
        <EducationEntry
          title="Colegio Arubano"
          subtitle="VWO N&T"
          content={bsc_content}
          image={colegio_logo}
        ></EducationEntry>
      </div>
    </div>
  );
}

function EducationEntry(props: {
  title: String;
  subtitle?: String;
  content: String;
  image?: any;
}) {
  return (
    <LeftEntranceAnimation>
      <div className="bg-tartorange my-8 h-auto w-1/2 flex flex-row p-3 rounded-md shadow-2xl">
        <img
          src={props.image}
          className="object-contain h-auto w-1/5 mr-3"
        ></img>
        <div className="flex flex-col justify-start ">
          <span className="text-2xl font-bold	float-left">{props.title}</span>
          <span className="text-l font-semibold float-left text-gray-800 ">
            {props.subtitle}
          </span>
          <span className="text-gray-500">{props.content}</span>
        </div>
      </div>
    </LeftEntranceAnimation>
  );
}

export default Education;
