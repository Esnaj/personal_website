import "./css/portfolio.scss";
import { projects_text } from "../text/projects";
import { availableLangs, useLanguage } from "../LanguageContext";
import { forwardRef } from "react";

export const Portfolio = forwardRef<HTMLDivElement>((props, ref) => {
    const currentLang = useLanguage() as availableLangs;
    return (
        <div className="w-full font-avenir" ref={ref}>
            <h1 className="text-5xl text-center my-16 text-gray-700">
                {projects_text.title[currentLang]}
            </h1>
            <div id="projects" className="flex flex-col items-center w-full space-y-20">
                {projects_text["projects"].map((e, i) => {
                    return (
                        <Project
                            title={e.name[currentLang]}
                            description={e.description[currentLang]}
                            image={e.image}
                            index={i}
                            key={i}
                        />
                    );
                })}
            </div>
        </div>
    );

    function Project(props: { title: string; description: string; image: string; index: number }) {
        const classes = [
            props.index % 2 === 0 ? "xl:flex-row" : "xl:flex-row-reverse",
            "flex flex-col space-y-3 x;:space-y-0 space-y-3 justify-between xl:w-3/4 project",
        ].join(" ");

        return (
            <div className={classes}>
                <div className="flex flex-col justify-center  items-center space-y-1 w-full px-10">
                    <h2 className="text-2xl text-center text-gray-700">{props.title}</h2>
                    <div className="bg-gray-400 line-split" />
                    <span className="text-center text-gray-600">{props.description}</span>
                </div>
                <div className="flex flex-row justify-center items-center w-full">
                    <img
                        src={props.image}
                        alt={props.title}
                        className=" m-0 project-image rounded-lg "
                    />
                </div>
            </div>
        );
    }
});
