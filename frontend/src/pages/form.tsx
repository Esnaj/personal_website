import { ErrorMessage, Field, Form, Formik } from "formik";
import { postForm } from "../api/emailer";
import { useLanguage, availableLangs } from "../LanguageContext";
import { contact } from "../text/projectentries";
import * as yup from "yup";
import { forwardRef } from "react";

export type formType = {
    name: string;
    email: string;
    company: string;
    phone: string;
    message: string;
};

export const ContactForm = forwardRef<HTMLDivElement>((props, ref) => {
    const currentLang = useLanguage() as availableLangs;
    const formSchema = yup.object().shape({
        name: yup.string().required(contact["error"]["name"][currentLang]),
        email: yup.string().email().required(contact["error"]["email"][currentLang]),
        company: yup.string(),
        phone: yup.string().matches(/^\d+$/, contact["error"]["phone"][currentLang]),
        message: yup.string().required(contact["error"]["message"][currentLang]),
    });

    return (
        <div ref={ref} className="bg-gray-200 rounded-sm w-full md:w-full my-20 p-8 md:p-10">
            <Formik
                initialValues={{
                    name: "",
                    company: "",
                    email: "",
                    phone: "",
                    message: "",
                }}
                onSubmit={(values, { resetForm, setSubmitting }) => {
                    setSubmitting(true);
                    postForm(values).then(() => {
                        resetForm();
                        setSubmitting(false);
                    });
                }}
                validationSchema={formSchema}>
                {({ isSubmitting, errors }) => {
                    return (
                        <Form>
                            <div className="flex flex-col space-y-3">
                                <h1 className="text-2xl text-gray-600">Contact</h1>
                                <div className="flex flex-col space-y-3 md:space-y-0 md:grid md:grid-cols-2 md:space-x-0 md:gap-x-2 gap-y-5">
                                    <MyField name="name" />
                                    <MyField name="company" />
                                    <MyField name="email" />
                                    <MyField name="phone" />
                                </div>
                                <Field
                                    name="message"
                                    placeholder={contact["placeholders"]["message"][currentLang]}
                                    as="textarea"
                                    className="px-3 py-2 h-20 border border-gray-300 rounded-sm shadow-sm focus:ring-2 focus:ring-blue-300 outline-none  w-full"
                                />
                                <ErrorMessage
                                    name="message"
                                    render={(msg) => <ErrorBox msg={msg} />}
                                />
                                <button
                                    type="submit"
                                    className="px-3 py-1 bg-blue-500 self-end w-24 text-white rounded-sm shadow-sm hover:bg-blue-700"
                                    disabled={isSubmitting}>
                                    {contact["placeholders"]["submit"][currentLang]}
                                </button>
                            </div>
                        </Form>
                    );
                }}
            </Formik>
        </div>
    );

    function MyField(props: { name: "name" | "company" | "email" | "phone" }) {
        return (
            <div className="md:full relative">
                <Field
                    type="input"
                    name={props.name}
                    placeholder={contact["placeholders"][props.name][currentLang]}
                    className="border w-full border-gray-300 h-10 rounded-sm shadow-sm focus:ring-2 focus:ring-blue-300 outline-none px-3"
                />
                <ErrorMessage name={props.name} render={(msg) => <ErrorBox msg={msg} />} />
            </div>
        );
    }
    function ErrorBox(props: { msg: string }) {
        return <div className="relative px-3 text-red-500">{props.msg}</div>;
    }
});
