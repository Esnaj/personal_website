export const home_text = {
    EN: {
        webapp: "Web application",
        needed: "needed",
        decription: "Esnaj software delivers top-level software for your project.",
        contact_button: "Email.",
        dlvr1: "delivers",
        dlvr2: "top-level",
        dlvr3: "software for your project.",
    },
    NL: {
        webapp: "Web applicatie",
        needed: "nodig",
        decription: "Esnaj software biedt top-niveau software aan voor uw project.",
        contact_button: "Email",
        dlvr1: "biedt",
        dlvr2: "top-niveau",
        dlvr3: "software aan voor uw project.",
    },
};

export const navbar_text = {
    EN: {
        home: "Home",
        services: "Services",
        projects: "Projects",
        about: "About",
        contact: "Contact",
    },
    NL: {
        home: "Thuis",
        services: "Diensten",
        projects: "Projecten",
        about: "Over",
        contact: "Contact",
    },
};

export const about_text = {
    EN: {
        text: 'Hi, I\'m Henky Janse, a software engineer living in Delft.'
    } ,
    NL: {
        text: 'Dag, ik ben Henky Janse, woon in Delft en ben software ingenieur.'
    }
}
