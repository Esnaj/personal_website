import { useState } from "react";
import { projectData, projectType } from "../text/projectentries";
import { AnimatePresence, motion } from "framer-motion";
import { useWindowDimensions } from "../components/Utils";
import { availableLangs, useLanguage } from "../LanguageContext";

export default function Projects() {
    // const [expandedProjectIndex, setExpandedProjectIndex] = useState(null);
    return (
        <div className="flex flex-col w-full mt-20 items-center h-screen">
            <p className=" text-5xl mb-3">Projects</p>
            <div className="grid grid-cols-2 md:grid-cols-4 gap-4 w-full p-3">
                {projectData.map((e) => {
                    return <ProjectEntry project={e} />;
                })}
            </div>
        </div>
    );
}

function ProjectEntry(props: { project: projectType }) {
    const [isExtended, setIsExtended] = useState(false);
    const currentLang = useLanguage() as availableLangs

    const { width } = useWindowDimensions();

    const hoverMotion = {
        hover: {
            scale: 1.1,
        },
    };
    return (
        <motion.div
            whileHover="hover"
            // onClick={() => setIsExtended(!isExtended)}
            className="w-auto col-span-1 relative flex flex-col items-center justify-center text-center focus:outline-none">
            <div
                className="flex flex-col border border-gray-200 p-2 items-center h-full w-full bg-gray-100 rounded-sm shadow-lg"
                onClick={() => setIsExtended(!isExtended)}>
                <div className={"h-10 w-10"}>
                    <motion.img
                        variants={hoverMotion}
                        src={props.project.image}
                        className=""></motion.img>
                </div>
                <p className="text-xl text-gray-700 font-bold	float-left">
                    {props.project.title[currentLang]}
                </p>
                {isMobile() && (
                    <button
                        onClick={() => setIsExtended(!isExtended)}
                        className="mt-auto self-end w-full flex flex-col items-center">
                        <hr className="border border-gray-300 w-10/12 font" />
                        <div className="w-1/2 text-gray-600">{isExtended ? "^" : "v"}</div>
                    </button>
                )}
            </div>
            <AnimatePresence>{isExtended && <CardPopup />}</AnimatePresence>
        </motion.div>
    );
    function Stack() {
        return (
            <div className="">
                <span className="text-sm font-semibold">Stack: </span>
                <span>{props.project.stack.join(", ")}</span>
            </div>
        );
    }

    function CardPopup() {
        const mobileCss =
            "h-full w-full fixed m-0 top-0 left-0 bg-gray-200 p-10 bg-opacity-80 flex flex-col justify-center items-center z-50";
        const normalCss = "relative w-2/1 self-start z-10";

        return (
            <motion.div
                initial={{ opacity: 0, y: -10 }}
                animate={{ opacity: 1, y: 0 }}
                exit={{ opacity: 0, y: -10 }}
                className={isMobile() ? normalCss : mobileCss}
                onClick={() => {
                    if (!isMobile()) {
                        setIsExtended(false)
                    }
                }}>
                <div
                    className="bg-white border-2 rounded-md pb-2 px-3 pt-0 flex flex-col items-start md:absolute md:top-5"
                    onBlur={() => setIsExtended(!isExtended)}>
                    <div className=" w-full relative mt-1 right-0 h-8 flex flex-row space justify-between items-center">
                        <button
                            className="left-0 top-1 text-red-500 h-3 w-3 "
                            onClick={() => setIsExtended(!isExtended)}>
                            <img src="../icons/close.svg" alt="closeIcon" />
                        </button>
                        {props.project.link && (
                            <a
                                href={props.project.link}
                                target="_blank"
                                rel="noreferrer"
                                className="right-2 h-4 w-4 fill-current text-gray-300 z-20">
                                <img src="../icons/exlink.svg" alt="link" />
                            </a>
                        )}
                    </div>

                    <Stack />
                    <hr className="w-11/12 border self-center" />
                    <span className="text-justify text-sm">{props.project.description[currentLang]}</span>
                </div>
            </motion.div>
        );
    }

    function isMobile() {
        return width > 768;
    }
}
