import React, { createRef, ReactChild, useEffect, useRef, useState } from "react";

export function LeftEntranceAnimation(props: { children: React.ReactChild }) {
    return <EntranceAnimation side="animate-fadeInLeft">{props.children}</EntranceAnimation>;
}

export function RightEntranceAnimation(props: { children: React.ReactChild }) {
    return <EntranceAnimation side="animate-fadeInRight">{props.children}</EntranceAnimation>;
}

function EntranceAnimation(props: { side: string; children: React.ReactChild }) {
    const [inView, setInView] = useState(false);
    const sideRef = createRef<HTMLDivElement>();
    var animation = props.side;
    var in_view_classes = [animation];
    var hidden_classes = ["invisible"];
    var current_css_classes = hidden_classes;
    if (inView) {
        current_css_classes = in_view_classes;
    } else {
        current_css_classes = hidden_classes;
    }

    const scrollHandler = () => {
        if (sideRef !== null && sideRef.current !== null) {
            const top = sideRef.current.getBoundingClientRect().top;
            const height = sideRef.current.getBoundingClientRect().height;
            setInView(inViewHandler(top, height));
        }
    };

    function inViewHandler(top: number, height: number) {
        let heightfactor = inView ? -0.3 : 1.05;
        return top + heightfactor * height <= window.innerHeight;
    }

    useEffect(() => {
        window.addEventListener("scroll", scrollHandler);
    });

    return (
        <div ref={sideRef} className={current_css_classes.join(" ")}>
            {props.children}
        </div>
    );
}
