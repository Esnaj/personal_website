module.exports = {
    purge: ["./src/**/*.{js,jsx,ts,tsx}", "./public/index.html"],
    darkMode: false, // or 'media' or 'class'
    theme: {
        minHeight: {

            '0': '0',

            '1/4': '25%',
            '1/6': '11%',
            '1/3': '33%',
            '2/3': '66%',
            '1/2': '50%',

            '3/4': '75%',

            '2/1': '200%',

            'full': '100%',
        },
        minWidth: {

            '0': '0',

            '1/4': '25%',

            '1/2': '50%',
            '2/3': '66%',

            '3/4': '75%',

            'full': '100%',
        },
        fontFamily: {
            avenir: ['avenir light', 'sans-serif']
        },
        extend: {
            fontFamily: {
                bodoni: "'Bodoni Moda', serif",
                lora: "'Lora', serif",
            },
            colors: {
                saffron: {
                    DEFAULT: "#EAC435",
                },
                beblue: {
                    DEFAULT: "#345995",
                },
                razzred: {
                    DEFAULT: "#E40066",
                },
                caribgreen: {
                    DEFAULT: "#03CEA4",
                },
                tartorange: {
                    DEFAULT: "#FB4D3D",
                },
                primary: {
                    DEFAULT: "#0758b7",
                },
                secondary: {
                    DEFAULT: "#039cfd",
                },
            },
            height: {
                '1/2vh': '50vh',
            },
            width: {
                '2/1': '200%',
                '1/2vw': '50vh',
            },
            animation: {
                fadeIn: "fadeIn 0.5s ease-out",
                fadeOut: "fadeOut 0.5s ease-out",
                fadeInLeft: "fadeInLeft 0.5s ease-out",
                fadeInRight: "fadeInRight 0.5s ease-out",
            },
            keyframes: {
                fadeInRight: {
                    "0%": { opacity: 0, transform: "translateX(5vw)" },

                    "100%": { opacity: 1, transform: "translateX(0vw)" },
                },
                fadeInLeft: {
                    "0%": { opacity: 0, transform: "translateX(-5vw)" },

                    "100%": { opacity: 1, transform: "translateX(0vw)" },
                },
                fadeIn: {
                    "0%": { opacity: 0, transform: "translateX(-5vw)" },

                    "100%": { opacity: 1, transform: "translateX(0vw)" },
                },
                fadeOutRight: {
                    "0%": { opacity: 1, transform: "translateX(0)" },

                    "100%": { opacity: 0, transform: "translateX(5vw)" },
                },
            },
        },
    },
    variants: {
        extend: {},
    },
    plugins: [],
};