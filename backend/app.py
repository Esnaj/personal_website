import logging
import smtplib
import ssl
from flask import Flask, request, Response, abort
import json
from flask_cors import CORS, cross_origin
from textwrap import dedent
import os
from dotenv import load_dotenv


app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'
app.config['TESTING'] = True

load_dotenv()


@app.route('/api/', methods=['POST'])
@cross_origin()
def send_mails():
    logging.basicConfig(level=logging.DEBUG)
    logging.info("request made")
    logging.info('Testing is: ' + str(app.config['TESTING']))
    try:
        request_data = json.loads(request.data)
    except Exception as e:
        abort(400, {'message': "Request doesn't contain data!"})

    try:
        emailer(request_data)
    except Exception as e:
        return Response(status=400, headers={'message': e})
    logging.info('request complete')
    return Response(status=200)


def json_validator(f):
    def wrapee(*args, **kwargs):
        must_have_keys = set(["email", "name", "message", "company", "phone"])
        print(args)
        data = args[0]
        keys = set(data.keys())
        if keys ^ must_have_keys == 0:
            error_message = "Request keys: " + str(keys) + \
                "; Are not equal to required keys: "\
                + str(must_have_keys)
            logging.error(error_message)
            abort(400, {'message': error_message})
        f(*args, **kwargs)
    return wrapee


@json_validator
def emailer(request_data) -> bool:
    logging.basicConfig(level=logging.DEBUG)
    port = os.getenv("PORT")
    context = ssl.create_default_context()
    sender_email = os.getenv('EMAIL')
    email_alias = os.getenv('EMAIL_ALIAS')
    password = os.getenv('EMAIL_PASSWORD')
    smtp_server = os.getenv('SMTP_SERVER_ADDR')
    logging.info("sending email with " + str(sender_email))

    with smtplib.SMTP_SSL(smtp_server, port, context=context) as server:
        subject = "Email Confirmation Esnaj Software"
        text = """Geachte {} \n\nUw email is goed aangekomen.
         \nMvg, \nHendrik Janse \n \n
        From: {}
        Company: {}
        Phone: {} \n
        message:
        {}
         """.format(request_data['name'], request_data['name'], request_data['company'], request_data['phone'], request_data['message'])

        msg = "Subject: {}\n\n{}".format(subject, dedent(text))
        try:
            server.login(sender_email, password)
            logging.info("login success!")
            logging.info("Sedning EMAIL to : " + request_data["email"])
            server.sendmail(from_addr=email_alias, to_addrs=[
                            request_data['email'], sender_email], msg=msg)
            logging.info("sentmail success!")
            return True

        except Exception as e:
            logging.error(e)
            abort(500, "Something went wrong with the sending of the email! \n" + str(e))
        finally:
            return False
